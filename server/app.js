const express = require('express')
const graphqlHTTP = require('express-graphql');
const bodyParser = require('body-parser')
const path = require('path')
const app = express()
const cors = require('cors')

const schema = require('./db/shema/shema');

app.use(cors())

app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, 'build')))

app.use('/graphql', graphqlHTTP({
    schema, 
    graphiql: true
}))

app.get('/ping', (req, res) => {
  res.send('pong');
})

module.exports = app;

