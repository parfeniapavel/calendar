const app = require('./app')
const Mongodb = require('./db/mongodb')

const PORT = process.env.PORT || 8080

const mongoClient = new Mongodb();

app.listen(PORT, () => console.log(`Listening on port ${PORT}`))

mongoClient.connect();