const mongoose = require('mongoose')

class Connector {
    constructor(){
        this.__connector = null
        this.__connectionString = "mongodb+srv://root:root@calendar-claster-orgbl.gcp.mongodb.net/calendar?retryWrites=true&w=majority"
    }

    connect() {
        this.__connector = mongoose.connect(this.__connectionString,
                {
                    useUnifiedTopology: true,
                    useNewUrlParser: true,
                }
            )
            .then(res => console.log('DB Connected!'))
            .catch(err => console.log(`DB Connection Error: ${err.message}`))
    }
}

module.exports = Connector;