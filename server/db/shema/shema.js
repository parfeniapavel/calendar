const graphql = require('graphql');

const {
    GraphQLObjectType, 
    GraphQLString, 
    GraphQLBoolean, 
    GraphQLList, 
    GraphQLID, 
    GraphQLNonNull, 
    GraphQLSchema
} = graphql;

const Users = require('../models/user');
const Todos = require('../models/todo');

const TodoType = new GraphQLObjectType({
    name: 'todos',
    fields: () => ({
        id: {type: GraphQLID},
        date: {type: GraphQLString},
        isCompleted: {type: GraphQLBoolean},
        time: {type: GraphQLString},
        title: {type: GraphQLString},
        text: {type: GraphQLString},
        userId: {type: GraphQLID},
        user: {
            type: UserType,
            resolve(parent, args){
                return Users.findById(parent.userId);
            }
        }
    })
});

const UserType = new GraphQLObjectType({
  name: 'users',
  fields: () => ({
    id: {type: GraphQLID},
    name: {type: GraphQLString},
    login: {type: GraphQLString},
    password: {type: GraphQLString},
    todos: {
      type: new GraphQLList(TodoType),
      resolve(parent, args){
        return Todos.find({userId: parent.id});
      }
    }
  }),
});

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addUser: {
            type: UserType,
            args: {
                name: { type: new GraphQLNonNull(GraphQLString) },
                login: { type: new GraphQLNonNull(GraphQLString) },
                password: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve: async (parent, args) => {
              const users = await Users.find({login: args.login})
              console.log(users)
              if(users.length) {
                throw new Error('User already exist!')
              }
              const result = await new Promise((resolve) => {
                setTimeout(() =>
                  resolve({
                    ...args
                  }), 100);
              });
              return result;
            }
        },
        addTodo: {
            type: TodoType,
            args: {
                isCompleted: { type: new GraphQLNonNull(GraphQLBoolean) },
                date: { type: new GraphQLNonNull(GraphQLString) },
                title: { type: new GraphQLNonNull(GraphQLString) },
                text: { type: new GraphQLNonNull(GraphQLString) },
                time: { type: new GraphQLNonNull(GraphQLString) },
                userId: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args){
                const todo = new Todos({
                    isCompleted: args.isCompleted,
                    date: args.date,
                    title: args.title,
                    text: args.text,
                    time: args.time,
                    userId: args.userId 
                });
                return todo.save();
            }
        },
        deleteUser: {
            type: UserType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args){
               return Users.findByIdAndRemove(args.id);
            }
        },
        deleteTodo: {
            type: TodoType,
            args: { id: { type: GraphQLID } },
            resolve(parent, args){
               return Todos.findByIdAndRemove(args.id);
            }
        },
        updateUser: {
            type: UserType,
            args: {
                id: { type: GraphQLID },
                name: { type: new GraphQLNonNull(GraphQLString) },
                login: { type: new GraphQLNonNull(GraphQLString) },
                password: { type: new GraphQLNonNull(GraphQLString) }
            },
            resolve(parent, args){
                return Users.findByIdAndUpdate(
                    args.id,
                    { $set: { name: args.name, login: args.login, password: args.password } },
                    { new: true }
                )
            }
        },
        updateTodo: {
            type: TodoType,
            args: {
                id: { type: GraphQLID },
                isCompleted: { type: new GraphQLNonNull(GraphQLBoolean) },
                date: { type: new GraphQLNonNull(GraphQLString) },
                title: { type: new GraphQLNonNull(GraphQLString) },
                text: { type: new GraphQLNonNull(GraphQLString) },
                time: { type: new GraphQLNonNull(GraphQLString) },
            },
            resolve(parent, args){
                return Todos.findByIdAndUpdate(
                    args.id,
                    {
                        $set: { 
                            isCompleted: args.isCompleted,
                            date: args.date,
                            title: args.title,
                            text: args.text,
                            time: args.time
                        }
                    },
                    { new: true }
                )
            }
        }
    }
})

const Query = new GraphQLObjectType({
    name: 'Query',
    fields: {
        todo: {
            type: TodoType,
            args: {id: {type: GraphQLID}},
            resolve(parent, args){
                return Todos.findById(args.id);
            }
        },
        user: {
            type: UserType,
            args: {
              login: {type: GraphQLString},
              password: {type: GraphQLString}
            },
            resolve(parent, args){
                return Users.findOne({login: args.login, password: args.password});
            }
        },
        todos: {
            type: new GraphQLList(TodoType),
            resolve(parent, args){
                return Todos.find({});
            }
        },
        userTodos: {
            type: new GraphQLList(TodoType),
            args: {userId: {type: GraphQLString}},
            resolve(parent, args){
                return Todos.find({userId: args.userId});
            }
        },
        users: {
            type: new GraphQLList(UserType),
            resolve(parent, args){
                return Users.find({});
            }
        }
    }
})

module.exports = new GraphQLSchema({
    query: Query,
    mutation: Mutation
})