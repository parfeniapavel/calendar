const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const todoSchema = new Schema({
    isCompleted: Boolean,
    date: String,
    title: String,
    text: String,
    time: String,
    userId: String
})

module.exports = mongoose.model('todos', todoSchema);