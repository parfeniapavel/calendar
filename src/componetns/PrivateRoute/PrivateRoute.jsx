import React from "react"
import { Route, Redirect } from "react-router-dom"

export default ({ component: Component, user, ...rest }) => (
  <Route
    {...rest}
    render={props =>
      user.isLogged ? (
        <Component {...props} />
      ) : (
        <Redirect to='/login' />
      )
    }
  />
)
