import { connect } from 'react-redux'
import PrivateRoute from './PrivateRoute'

export default connect((state) => ({ user: state.user }), null)(PrivateRoute)