import React from "react"

export default ({ user }) => <span>{user.userData ? user.userData.name : ''}</span>
