import React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PublicRoute = ({component: Component, user, restricted, ...rest}) => {
  return (
    <Route {...rest} render={props => (
      user.isLogged && restricted ?
        <Redirect to="/calendar" />
      : <Component {...props} />
    )} />
  );
};

export default PublicRoute;