import { connect } from 'react-redux'
import PublicRoute from './PublicRoute'

export default connect((state) => ({ user: state.user }), null)(PublicRoute)