import React from 'react'
import { Modal, Form, Input, DatePicker, TimePicker } from 'antd';
import moment from 'moment';

export default Form.create({ name: 'form_in_modal' })(
  class extends React.Component {

    handleSubmit = e => {
      e.preventDefault()

      this.props.form.validateFields((err, fieldsValue) => {
        if (err) {
          return
        }

        const values = {
          ...fieldsValue,
          'date-picker': fieldsValue['date-picker'].format('YYYYMMDD'),
          'time-picker': fieldsValue['time-picker'].format('HH:mm')
        }

        const todo = {
          isCompleted: false,
          date: values['date-picker'],
          time: values['time-picker'],
          text: values['description'],
          title: values['title']
        }

        this.props.handleTodoAdd(todo)
      });
    }

    render() {
      const { visible, onCancel, form, defaultData } = this.props
      const { getFieldDecorator } = form
      const { date = '', time = '', title = '', text = '' } = defaultData || {};

      const dateOptions = {
        rules: [
          {
            type: 'object', 
            required: true, 
            message: 'Please select date!'
          }
        ]
      }
      if(date) dateOptions.initialValue = moment(date)

      const timeOptions = {
        rules: [{ type: 'object', required: true, message: 'Please select time!' }]
      }
      if(date&&time) timeOptions.initialValue = moment(moment(date).format('YYYY-MM-DD')+' '+time)

      return (
        <Modal
          visible={visible}
          title="Add todo"
          okText="Add"
          onCancel={onCancel}
          onOk={this.handleSubmit}
        >
          <Form layout="vertical">
            <Form.Item label="Date">
              {getFieldDecorator('date-picker', dateOptions)(<DatePicker/>)}
            </Form.Item>
            <Form.Item label="Time">
              {getFieldDecorator('time-picker', timeOptions)(<TimePicker format='HH:mm'/>)}
            </Form.Item>
            <Form.Item label="Title">
              {getFieldDecorator('title', {
                rules: [{ required: true, message: 'Please input the title of todo!' }],
                initialValue: title
              })(<Input />)}
            </Form.Item>
            <Form.Item label="Description">
              {getFieldDecorator('description', {
                initialValue: text
              })(<Input type="textarea" />)}
            </Form.Item>
          </Form>
        </Modal>
      );
    }
  },
);

