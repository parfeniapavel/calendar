import React, { Fragment } from 'react';
import { Button, Icon, Typography } from 'antd'
import { Row } from './styled'
import moment from 'moment'

const { Title } = Typography;

export default ({handleAddButtonClick, date}) => {
  return (
    <Fragment>
      <Row>
        <Title>
          {moment(date).format('DD MMM')}
        </Title>
        <Button onClick={handleAddButtonClick}>
          <Icon type="plus"></Icon>
        </Button>
      </Row>
    </Fragment>
  );
}

