import React, { useState } from 'react';
import { List } from 'antd'
import debounce from 'debounce'
import Todo from '../Todo'
import AddTodoForm from '../AddTodoForm'
import TodosNavbar from '../TodosNavbar'

export default ({data, user, editTodo, addTodo, deleteTodo, date}) => {

  const [addTodoFormVisible, handleAddTodoFormVisible] = useState(false)

  const handleTodoChange = debounce((todo) => {
    editTodo(todo)
  }, 100)

  const handleTodoAdd = todo => {
    addTodo(user, todo)
    handleAddTodoFormVisible(false)
  }

  const handleTodoDelete = id => {
    deleteTodo(id)
  }

  return (
    <div style={{flexGrow: 2, maxWidth: '50%', minWidth: '340px'}}>
      <AddTodoForm
        defaultData={{date}}
        visible={addTodoFormVisible}
        onCancel={() => handleAddTodoFormVisible(false)}
        handleTodoAdd={handleTodoAdd}
      ></AddTodoForm>
      <TodosNavbar date={date} handleAddButtonClick={() => handleAddTodoFormVisible(true)}/>
      <List
        itemLayout="horizontal"
        dataSource={data}
        renderItem={todo => (
          <List.Item>
            <Todo data={todo} handleTodoChange={handleTodoChange} handleTodoDelete={handleTodoDelete}/>
          </List.Item>
        )}
      />
    </div>
  );
}

