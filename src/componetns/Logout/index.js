import { connect } from 'react-redux'
import Logout from './Logout'

import { user } from '../../actions'

const mapStateToProps = ({ user }) => {
  return {
    user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    userLogout: () => {
      dispatch(user.userLogout())
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Logout)