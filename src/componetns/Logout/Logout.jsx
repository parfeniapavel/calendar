import React from "react"
import { Button } from 'antd'

export default ({ user, userLogout }) => user.isLogged ? <Button onClick={() => userLogout()}>Log out</Button> : null
