import styled from 'styled-components'

const Row = styled.div`
  display: flex;
  width: 100%;
`
const Time = styled.span`
  display: inline-block;
  cursor: pointer;
  width: 40px;
`

export {
  Row,
  Time
}