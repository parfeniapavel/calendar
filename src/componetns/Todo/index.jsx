import React, { Fragment, useState } from 'react'
import { List, Checkbox, Button, Icon } from 'antd'
import { Row, Time } from './styled'
import { AddTodoForm } from '../'

const ButtonGroup = Button.Group;

export default ({data, handleTodoChange, handleTodoDelete}) => {

  const {id, isCompleted, time, title, text} = data
  const [isEditable, handleEditable] = useState(false)

  const handleEditTodo = (todo) => {
    handleEditable(false)
    handleTodoChange(
      { ...data, ...todo }
    )
    console.log('asd')
  }

  return (
    <Fragment>
      <Row>
        <AddTodoForm
          defaultData={data}
          visible={isEditable}
          onCancel={() => handleEditable(false)}
          handleTodoAdd={handleEditTodo}
        ></AddTodoForm>
        <List.Item.Meta
          avatar={<Checkbox checked={isCompleted} onChange={() => handleTodoChange({ ...data, isCompleted: !isCompleted })}/>}
          title={
            <Fragment>
              <Time>{time}</Time>
              {title}
            </Fragment>
          }
          description={
            <span style={{marginLeft: '40px'}}>{text}</span>  
          }
        />
        <ButtonGroup>
          <Button onClick={() => handleEditable(true)} disabled={isCompleted}>
            <Icon type="edit" />
          </Button>
          <Button onClick={() => handleTodoDelete(id)} disabled={isCompleted}>
            <Icon type="delete" />
          </Button>
        </ButtonGroup>
      </Row>
    </Fragment>
  );
}

