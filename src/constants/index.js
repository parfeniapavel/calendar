
/*                    TODOS                                               */
export const ACTION_GET_TODOS = 'ACTION_GET_TODOS'
export const ACTION_GET_TODOS_SUCCESS = 'ACTION_GET_TODOS_SUCCESS'
export const ACTION_GET_TODOS_ERROR = 'ACTION_GET_TODOS_ERROR'

export const ACTION_EDIT_TODO = 'ACTION_EDIT_TODO'
export const ACTION_EDIT_TODO_SUCCESS = 'ACTION_EDIT_TODO_SUCCESS'
export const ACTION_EDIT_TODO_ERROR = 'ACTION_EDIT_TODO_ERROR'

export const ACTION_ADD_TODO = 'ACTION_ADD_TODO'
export const ACTION_ADD_TODO_SUCCESS = 'ACTION_ADD_TODO_SUCCESS'
export const ACTION_ADD_TODO_ERROR = 'ACTION_ADD_TODO_ERROR'

export const ACTION_DELETE_TODO = 'ACTION_DELETE_TODO'
export const ACTION_DELETE_TODO_SUCCESS = 'ACTION_DELETE_TODO_SUCCESS'
export const ACTION_DELETE_TODO_ERROR = 'ACTION_DELETE_TODO_ERROR'
/*                    /TODOS                                               */

/*                    AUTH                                               */
export const ACTION_USER_LOGIN = 'ACTION_USER_LOGIN'
export const ACTION_USER_LOGIN_SUCCESS = 'ACTION_USER_LOGIN_SUCCESS'
export const ACTION_USER_LOGIN_ERROR = 'ACTION_USER_LOGIN_ERROR'

export const ACTION_USER_LOGOUT_SUCCESS = 'ACTION_USER_LOGOUT_SUCCESS'

export const ACTION_USER_SIGN_IN_SUCCESS = 'ACTION_USER_SIGN_IN_SUCCESS'
export const ACTION_USER_SIGN_IN_ERROR = 'ACTION_USER_SIGN_IN_ERROR'
/*                    /AUTH                                               */