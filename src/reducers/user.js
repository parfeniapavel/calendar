import { ACTION_USER_LOGIN_SUCCESS, ACTION_USER_LOGOUT_SUCCESS, ACTION_USER_SIGN_IN_SUCCESS } from '../constants'

const userData = JSON.parse(localStorage.getItem('userData'))

const initialState = {
  isLogged: !!userData,
  userData
}

export default function userReducer (state = initialState, action) {
  switch (action.type) {
    case ACTION_USER_LOGIN_SUCCESS:
      return {
        isLogged: true,
        userData: action.payload
      }

    case ACTION_USER_LOGOUT_SUCCESS:
      return {
        isLogged: false,
        userData: action.payload
      }

    case ACTION_USER_SIGN_IN_SUCCESS:
      return {
        isLogged: true,
        userData: action.payload
      }

    default:
      return state
  }
}