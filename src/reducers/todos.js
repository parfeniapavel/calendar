import { ACTION_GET_TODOS_SUCCESS, ACTION_EDIT_TODO_SUCCESS, ACTION_ADD_TODO_SUCCESS, ACTION_DELETE_TODO_SUCCESS } from '../constants'

export default function postsReducer (state = [], action) {
  switch (action.type) {
    case ACTION_GET_TODOS_SUCCESS:
      return action.payload

    case ACTION_EDIT_TODO_SUCCESS:
      return state.map(todo => {
        if(todo.id === action.payload.id) {
          return action.payload
        } else {
          return todo
        }
      })

    case ACTION_ADD_TODO_SUCCESS:
      return [...state, action.payload]

    case ACTION_DELETE_TODO_SUCCESS:
      return state.filter(({id}) => id !== action.payload.id)

    default:
      return state
  }
}