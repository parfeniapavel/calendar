import { connect } from 'react-redux'
import RegistrationPage from './RegistrationPage'

import { user } from '../../actions'

const mapDispatchToProps = dispatch => {
  return {
    addUser: (name, login, password) => {
      dispatch(user.addUser(name, login, password))
    }
  }
}

export default connect(null, mapDispatchToProps)(RegistrationPage)