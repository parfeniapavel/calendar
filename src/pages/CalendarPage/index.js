import { connect } from 'react-redux'
import CalendarPage from './CalendarPage'

import { todos } from '../../actions'

const mapDispatchToProps = dispatch => {
  return {
    getTodos: userId => {
      dispatch(todos.getTodos(userId))
    }
  }
}

export default connect(({ todos, user }) => {return {todos, user}}, mapDispatchToProps)(CalendarPage)