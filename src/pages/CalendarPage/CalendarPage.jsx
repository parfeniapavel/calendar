import React, { Component } from 'react'
import { Calendar } from 'antd';
import moment from 'moment'

export default class CalendarPage extends Component {
  userId = this.props.user.userData.id;

  componentDidMount() {
    this.props.getTodos(this.userId)
  }
  
  dateCellRender = date => {
    const { todos } = this.props;

    if(todos.find(todo => moment(todo.date).format('YMMDD') === date.format('YMMDD'))) {
      return (<span>&#8254;</span>)
    }
  }

  render() {
    return (
      <Calendar
        fullscreen={false}
        dateCellRender={this.dateCellRender}
        style={{width: 360}}
        onSelect={date => this.props.history.push(`/calendar/${date.format('YMMDD')}`)}
      />
    )
  }
}