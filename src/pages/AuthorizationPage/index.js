import { connect } from 'react-redux'
import AuthorizationPage from './AuthorizationPage'

import { user } from '../../actions'

const mapStateToProps = ({ user }) => {
  return {
    user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    userLogin: (login, password) => {
      dispatch(user.userLogin(login, password))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(AuthorizationPage)