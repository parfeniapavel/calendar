import React, { Component } from 'react'
import { Todos } from '../../componetns'

export default class TodosPage extends Component {
  userId = this.props.user.userData.id;

  componentDidMount() {
    this.props.getTodos(this.userId)
  }

  render() {
    const { todos, editTodo, addTodo, deleteTodo } = this.props
    const { date } = this.props.match.params

    return (
      <Todos 
        date={date}
        user={this.userId}
        data={todos.filter(todo => todo.date === date).sort((a,b) => a.date > b.date ? 1 : 0)}
        editTodo={editTodo}
        addTodo={addTodo}
        deleteTodo={deleteTodo}
      />
    )
  }
}