import { connect } from 'react-redux'
import TodosPage from './TodosPage'

import { todos } from '../../actions'

const mapStateToProps = ({ todos, user }) => {
  return {
    todos,
    user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getTodos: userId => {
      dispatch(todos.getTodos(userId))
    },
    editTodo: todo => {
      dispatch(todos.editTodo(todo))
    },
    addTodo: (userId, todo) => {
      dispatch(todos.addTodo(userId, todo))
    },
    deleteTodo: id => {
      dispatch(todos.deleteTodo(id))
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TodosPage)