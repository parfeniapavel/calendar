import React, { Fragment } from 'react';
import { PrivateRoute } from '../../componetns'

import { TodosPage, CalendarPage } from '../'

function TimesheetPage() {
  return (
    <Fragment>
      <PrivateRoute path="/calendar" component={ CalendarPage } />
      <PrivateRoute path="/calendar/:date" component={ TodosPage } ></PrivateRoute>
    </Fragment>
  );
}

export default TimesheetPage;
