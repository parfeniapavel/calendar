import api from '../api'

import {
  ACTION_GET_TODOS_SUCCESS, 
  ACTION_GET_TODOS_ERROR, 
  ACTION_EDIT_TODO_SUCCESS, 
  ACTION_EDIT_TODO_ERROR,
  ACTION_ADD_TODO_SUCCESS, 
  ACTION_ADD_TODO_ERROR,
  ACTION_DELETE_TODO_SUCCESS, 
  ACTION_DELETE_TODO_ERROR,
} from '../constants'

export const getTodos = (userId) => {
  return async (dispatch) => {
    try{
      const response = await api(
        {
          method: 'post',
          url: '/graphql',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          }, 
          data: JSON.stringify({
            query: `
            query ($userId: String!){
              userTodos (userId: $userId){
                id,
                isCompleted,
                date,
                time,
                title,
                text
              }
            }`,
            variables: {
              userId
            }
          })
        }
      )

      dispatch({ type: ACTION_GET_TODOS_SUCCESS , payload: response.data.data.userTodos})
    }
    catch (error){
      dispatch({ type: ACTION_GET_TODOS_ERROR , payload: error})
    }
  }
}

export const editTodo = ({ id, isCompleted, date, time, text, title }) => {
  return async (dispatch) => {
    try{
      const response = await api(
        {
          method: 'post',
          url: '/graphql',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          }, 
          data: JSON.stringify({
            query: `
            mutation ($id: ID!, $isCompleted: Boolean!, $date: String!, $time: String!, $text: String!, $title: String!){
              updateTodo(id: $id, isCompleted: $isCompleted, date: $date, time: $time, text: $text, title: $title){
                id,
                isCompleted,
                date,
                time,
                text,
                title
              }
            }`,
            variables: {
              id,
              isCompleted,
              date,
              time,
              text,
              title
            }
          })
        }
      )
      dispatch({ type: ACTION_EDIT_TODO_SUCCESS , payload: response.data.data.updateTodo})
    }
    catch (error){
      console.log('tyt', error);
      dispatch({ type: ACTION_EDIT_TODO_ERROR , payload: false})
    }
  }
}

export const addTodo = (userId, { isCompleted, date, time, text, title }) => {
  return async (dispatch) => {
    try{
      console.log(userId, { isCompleted, date, time, text, title })
      const response = await api(
        {
          method: 'post',
          url: '/graphql',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          }, 
          data: JSON.stringify({
            query: `
            mutation ($isCompleted: Boolean!, $date: String!, $time: String!, $text: String!, $title: String!, $userId: String!){
              addTodo(isCompleted: $isCompleted, date: $date, time: $time, text: $text, title: $title, userId: $userId){
                id,
                isCompleted,
                date,
                time,
                text,
                title
              }
            }`,
            variables: {
              isCompleted,
              date,
              time,
              text,
              title,
              userId
            }
          })
        }
      )
      dispatch({ type: ACTION_ADD_TODO_SUCCESS , payload: response.data.data.addTodo})
    }
    catch (error){
      console.log(error);
      dispatch({ type: ACTION_ADD_TODO_ERROR , payload: false})
    }
  }
}

export const deleteTodo = (id) => {
  return async (dispatch) => {
    try{
      const response = await api(
        {
          method: 'post',
          url: '/graphql',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          }, 
          data: JSON.stringify({
            query: `
            mutation ($id: ID!){
              deleteTodo(id: $id){
                id
              }
            }`,
            variables: {
              id
            }
          })
        }
      )
      dispatch({ type: ACTION_DELETE_TODO_SUCCESS , payload: response.data.data.deleteTodo})
    }
    catch (error){
      console.log(error);
      dispatch({ type: ACTION_DELETE_TODO_ERROR , payload: false})
    }
  }
}