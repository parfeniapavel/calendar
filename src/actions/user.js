import api from '../api'

import {
  ACTION_USER_LOGIN_SUCCESS,
  ACTION_USER_LOGIN_ERROR,
  ACTION_USER_LOGOUT_SUCCESS,
  ACTION_USER_SIGN_IN_SUCCESS,
  ACTION_USER_SIGN_IN_ERROR
} from '../constants'

export const userLogin = (login, password) => {
  return async (dispatch) => {
    try{
      const response = await api(
        {
          method: 'post',
          url: '/graphql',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          }, 
          data: JSON.stringify({
            query: `
            query ($login: String!, $password: String!){
              user(login: $login, password: $password){
                id,
                name,
                login,
              }
            }`,
            variables: {
              login,
              password
            }
          })
        }
      )

      if(response.data.data.user){
        localStorage.setItem('userData', JSON.stringify(response.data.data.user))
        dispatch({ type: ACTION_USER_LOGIN_SUCCESS , payload: response.data.data.user })
      } else {
        localStorage.removeItem('userData')
        dispatch({ type: ACTION_USER_LOGIN_ERROR , payload: null })
      }
    }
    catch (error){
      dispatch({ type: ACTION_USER_LOGIN_ERROR , payload: error })
    }
  }
}

export const userLogout = () => {
  localStorage.removeItem('userData')
  return { type: ACTION_USER_LOGOUT_SUCCESS , payload: { isLogin: false }}
}

export const addUser = (name, login, password) => {
  return async (dispatch) => {
    try{
      const response = await api(
        {
          method: 'post',
          url: '/graphql',
          headers: {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
          }, 
          data: JSON.stringify({
            query: `
            mutation ($login: String!, $password: String!, $name: String!){
              addUser(login: $login, password: $password, name: $name){
                id,
                login,
                password,
                name
              }
            }`,
            variables: {
              name,
              login,
              password
            }
          })
        }
      )

      if(response.data.errors) {
        throw new Error(response.data.errors[0].message)
      }

      localStorage.setItem('userData', JSON.stringify(response.data.data.addUser))
      dispatch({ type: ACTION_USER_SIGN_IN_SUCCESS , payload: response.data.data.addUser })
    }
    catch (error){
      console.error(error)
      alert(error)
    }
  }
}