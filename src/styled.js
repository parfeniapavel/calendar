
import styled from 'styled-components'

const Container = styled.div`
  padding: 0 50px;
  display: flex;
  justify-content: space-around;
  flex-wrap: wrap;
`;

const Title = styled.h1`
  font-size: 1.5em;
  text-align: center;
  color: white;
`;

export {
  Container,
  Title
}