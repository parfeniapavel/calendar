import React from 'react';
import { Switch, Redirect } from 'react-router-dom'
import { Layout } from 'antd'
import { Container, Title } from './styled'
import { PrivateRoute, PublicRoute, Logout, Hello } from './componetns'

import { AuthorizationPage, RegistrationPage, TimesheetPage, NotFoundPage } from './pages'

import './App.css'

const { Header, Content, Footer } = Layout;

function App() {
  return (
    <div className="App">
      <Layout>
        <Header>
          <Title>Calendar <Hello/></Title>
        </Header>
        <Content>
          <Container className="app-container">
            <Switch>
              <Redirect exact from='/' to='/calendar' />
              <PrivateRoute path='/calendar' component={ TimesheetPage }/>
              <PublicRoute restricted={true} path='/login' component={ AuthorizationPage } />
              <PublicRoute exact restricted={true} path='/registration' component={ RegistrationPage } />
              <PublicRoute component={ NotFoundPage } />
            </Switch>
          </Container>
        </Content>
        <Footer>
          <Container>
            <Logout />
          </Container>
        </Footer>
      </Layout>
    </div>
  );
}

export default App;
